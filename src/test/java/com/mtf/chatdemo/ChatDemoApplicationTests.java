package com.mtf.chatdemo;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.Date;

//@SpringBootTest
class ChatDemoApplicationTests {

    @Test
    void contextLoads() {
    }

    /*
    * 生成Token令牌
    * */
    @Test
    public void generateToken(){
        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.SECOND,120);
        calendar.add(Calendar.DATE,7);

        //创建一个token
        String token = JWT.create()
                //设置负载信息
                .withClaim("username","matengfei")
                .withClaim("age",22)

                //令牌设置过期时间（什么时候过期）
                .withExpiresAt(calendar.getTime())

                //设置密钥，并指定以哪种算法来加密
                .sign(Algorithm.HMAC256("@matengfei"));

        System.out.println(token);
    }

    /*
    * 解析JWT
    * */
    @Test
    public void parseToken(){
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NzMwOTcyMDAsImFnZSI6MjIsInVzZXJuYW1lIjoibWF0ZW5nZmVpIn0.4LpIFA5tSpomxFIvuLUPSKi0BpZoo1ocYoGRkDkQs2E";
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256("@matengfei")).build();

        DecodedJWT decodedJWT = jwtVerifier.verify(token);
        System.out.println(decodedJWT);

        System.out.println("用户名："+decodedJWT.getClaim("username").asString());
        System.out.println("用户年龄："+decodedJWT.getClaim("age").asInt());
        System.out.println("token过期时间为："+decodedJWT.getExpiresAt());
    }

}
