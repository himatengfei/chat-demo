package com.mtf.chatdemo.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mtf.chatdemo.common.Message;
import com.mtf.chatdemo.entity.User;

public class MessageUtil {
    //将生成的Message对象转为Json字符串
    public static String generateJSONMessage(Boolean isSystem, String fromUserName, Object toUserName, Object message) throws JsonProcessingException {
        if (isSystem){
            //系统消息
            Message resultMessage = new Message(true, fromUserName, toUserName, message);
            ObjectMapper objectMapper = new ObjectMapper();
            String systemMessage = objectMapper.writeValueAsString(resultMessage);
            return systemMessage;
        }else {
            Message resultMessage = new Message(false, fromUserName, toUserName, message);
            ObjectMapper objectMapper = new ObjectMapper();
            String simpleMessage = objectMapper.writeValueAsString(resultMessage);
            return simpleMessage;
        }
    }
}
