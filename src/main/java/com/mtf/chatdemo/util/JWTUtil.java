package com.mtf.chatdemo.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;

/*
* JWT工具类
* */
@Slf4j
public class JWTUtil {
    //定义密钥用来“签名”，目的就是为了验证数据是否被篡改
    private final static String key = "@matengfei";


    //生成jwt令牌
    public static String generateToken(String userName){
        //设置token有效期，这里设置一周
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,7);

        //创建一个token
        String token = JWT.create()
                //设置负载信息
                .withClaim("userName",userName)
                //令牌设置过期时间（什么时候过期）
                .withExpiresAt(calendar.getTime())
                //设置密钥，并指定以哪种算法来加密
                .sign(Algorithm.HMAC256(key));
        log.info("生成的token令牌为：{}",token);
        return token;
    }

    //解析jwt令牌的负载信息
    public static String parseToken(String token){
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(key)).build();

        DecodedJWT decodedJWT = jwtVerifier.verify(token);
//        System.out.println(decodedJWT);
//        System.out.println("用户名："+decodedJWT.getClaim("username").asString());
//        System.out.println("用户年龄："+decodedJWT.getClaim("age").asInt());
//        System.out.println("token过期时间为："+decodedJWT.getExpiresAt());
        return decodedJWT.getClaim("userName").asString();
    }
}
