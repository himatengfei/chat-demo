package com.mtf.chatdemo.common;

import com.mtf.chatdemo.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//定义服务端要发送的消息格式
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private Boolean isSystem;
    private String fromUserName;
    private Object toUserName;
    private Object message;
}
