package com.mtf.chatdemo.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//返回结果类
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    //true:状态成功
    private Boolean flag;
    private String message;
    private Object object;

    //成功1
    public static Result successSimple(Object object){
        Result result = new Result();
        result.setFlag(true);
        result.setMessage((String) object);
        return result;
    }
    //成功2
    public static Result successDetail(Object object,String message){
        Result result = new Result();
        result.setFlag(true);
        result.setMessage(message);
        result.setObject(object);
        return result;
    }

    //失败
    public static Result errorSimple(String message){
        Result result = new Result();
        result.setFlag(false);
        result.setMessage(message);
        return result;
    }
}
