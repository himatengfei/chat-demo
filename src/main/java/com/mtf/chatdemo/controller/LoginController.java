package com.mtf.chatdemo.controller;

import com.mtf.chatdemo.common.Result;
import com.mtf.chatdemo.entity.User;
import com.mtf.chatdemo.util.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/user")
public class LoginController {
    //用户登录
    @PostMapping("/userLogin")
    public Result userLogin(@RequestBody User user){
        log.info("前端传递的登录信息为：{}",user.toString());

        String userName = user.getUserName();
        String userPassword = user.getUserPassword();

        if ("张三".equals(userName)&&"123456".equals(userPassword)){
            //1、生成token令牌，这里的作用主要是用来存放登录用户信息
            String token = JWTUtil.generateToken(userName);
//            user.setAvatar("pexels-aviv-perets-3274903.jpg");
            //2、登录成功，返回结果
            return Result.successDetail(token,"登录成功！");
        }else if ("李四".equals(userName)&&"123456".equals(userPassword)){
            String token = JWTUtil.generateToken(userName);
//            user.setAvatar("pexels-evgeny-tchebotarev-4101555.jpg");
            return Result.successDetail(token,"登录成功！");
        }else if ("王五".equals(userName)&&"123456".equals(userPassword)){
            String token = JWTUtil.generateToken(userName);
//            user.setAvatar("pexels-pixabay-210243.jpg");
            return Result.successDetail(token,"登录成功！");
        }else {
            return Result.errorSimple("用户名或密码错误！");
        }
    }

    //解析已经成功登录的用户信息（解析token令牌）
    @GetMapping("/getUserInfo/{token}")
    public String parseToken(@PathVariable("token") String token){
        return JWTUtil.parseToken(token);
    }
}
